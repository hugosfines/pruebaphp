<?php
$usuario = "root";
$contrasena = "";
$servidor = "localhost";
$basededatos = "intelcost_bienes";

$db = new mysqli($servidor, $usuario, "", $basededatos);
if ($db->connect_errno) {
    echo "Falló la conexión a MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
    exit;
}

if (!($stmt = $db->prepare("select * FROM bienes ORDER BY id"))) {
    echo "Falló la preparación: (" . $db->errno . ") " . $db->error;
}
/*$stmt->execute();
echo $stmt;*/
if (!$stmt->execute()) {
     echo "Falló la ejecución: (" . $stmt->errno . ") " . $stmt->error;
}

if (!($resultado = $stmt->get_result())) {
    echo "Falló la obtención del conjunto de resultados: (" . $stmt->errno . ") " . $stmt->error;
}
//$datos = $resultado->fetch_object();
$datos = [];
for ($num_fila = ($resultado->num_rows - 1); $num_fila >= 0; $num_fila--) {
    $resultado->data_seek($num_fila);
    $datos[] = $resultado->fetch_assoc();
}

echo json_encode($datos);
$stmt->close();
$db->close();
?>