<?php
$usuario = "root";
$contrasena = "";
$servidor = "localhost";
$basededatos = "intelcost_bienes";

$db = new mysqli($servidor, $usuario, "", $basededatos);
if ($db->connect_errno) {
    echo "Falló la conexión a MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
    exit;
}

if (!($stmt = $db->prepare("INSERT INTO bienes(direccion, ciudad, telefono, codigo_postal, tipo, precio) VALUES (?, ?, ?, ?, ?, ?)"))) {
    echo "Falló la preparación: (" . $db->errno . ") " . $db->error;
}
$stmt->bind_param('sssisd', $direccion, $ciudad, $telefono, $codigo_postal, $tipo, $precio);
$direccion = htmlspecialchars($_POST["direccion"]);
$ciudad = htmlspecialchars($_POST["ciudad"]);
$telefono = htmlspecialchars($_POST["telefono"]);
$codigo_postal = htmlspecialchars($_POST["codigo_postal"]);
$tipo = htmlspecialchars($_POST["tipo"]);
$precio = htmlspecialchars( preg_replace('/[^0-9]+/', '', $_POST["precio"]) );
$stmt->execute();
echo "Registro guardado con exito. ";
$stmt->close();
$db->close();

?>